# Ce dépôt

Dans ce dépôt, vous aurez accès à toutes les ressources utilisées dans les cours et TP sur le développement modulaire d'applications Web avec Java, en utilisant les Servlets, Spring MVC, Spring Boot, Testing, Security, WebFlux, ...

## Semaine 1 - Séance 1 :
Notions de base autour de la programmation Web avec Java
- [Cours 1](./c1_java_web/c1_java_web.pdf)
- [Cours 1 bis](./c1bis_maven/c1_maven.pdf)
- [TP 1](./tp1_java_web/tp1_java_web.pdf)


## Semaine 1 - Séance 2 :
Modulariser les applications Java avec Spring
- [Cours 2](./c2_spring_di/c2_spring_di.pdf)
- [TP 2](./tp2_spring_di/tp2_spring_di.pdf)

## Semaine 2 - Séances 3 et 4 :
Bien structurer une application Web avec Spring MVC
- [Cours 3 et TP 3](./c3_spring_mvc/c3_spring_mvc.pdf)

## Semaine 3 - Séances  5 et 6 :
Auto-configurer une application Web avec Spring Boot
- [Cours 4 et TP 4](./c4_spring_boot/c4_spring_boot.pdf)
- [Script de création des tables de la base de données](./c4_spring_boot/code/create_tables_covid.sql)
- [Dépôt de fichiers](https://moodle.umontpellier.fr/mod/assign/view.php?id=549232)